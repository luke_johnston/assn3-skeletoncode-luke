﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public partial class EditURLPage : ContentPage
	{

        private SQLiteAsyncConnection _connection;
        private ObservableCollection<WebURL> _url;
        private int SelectedID;


		public EditURLPage(WebURL selectedClass, int selection)
		{
			InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            BindingContext = selectedClass;
            SelectedID = selection;

        }

        async void EditURL(object sender, EventArgs e)
		{
            var url = await _connection.Table<WebURL>().ToListAsync();
            _url = new ObservableCollection<WebURL>(url);

            await DisplayAlert("Success", "URL Updated", "OK");

            var urlBlock = _url[SelectedID];
            urlBlock.title = TitleNew.Text;
            urlBlock.image = ImageNew.Text;
            urlBlock.url = URLNew.Text;

            await _connection.UpdateAsync(urlBlock);
        }

        async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}