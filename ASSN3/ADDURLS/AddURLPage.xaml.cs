﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public partial class AddURLPage : ContentPage
	{
		private SQLiteAsyncConnection _connection;

		public AddURLPage()
		{
			InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();

        }

        async void AddURL(object sender, EventArgs e)
		{
            var url = new WebURL { title = TitleNew.Text, image = ImageNew.Text, url = URLNew.Text };
            await _connection.InsertAsync(url);

            await DisplayAlert("Success", "URL Added", "OK" );

            TitleNew.Text = "";
            URLNew.Text = "";
            ImageNew.Text = "";
		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}

	}
}
